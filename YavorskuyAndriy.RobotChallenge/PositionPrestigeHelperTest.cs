﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace YavorskuyAndriy.RobotChallenge.Test
{

    [TestClass]
    public class TestPositionPrestigeHelper
    {
        [TestMethod]
        public void TestEvaluate()
        {
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60}
                }
            };
            var result = PositionPrestigeHelper.Evaluate(map);
            Assert.IsTrue(PositionPrestigeHelper.IsChecked);
            Assert.AreEqual(PositionHelper.PositionPrestigeList.First().EnergyProduce, PositionHelper.PositionPrestigeList.Max(x => x.EnergyProduce));
            Assert.IsNull(PositionHelper.PositionPrestigeList.FirstOrDefault(x => x.EnergyProduce == 0));
        }

        
    }
}
