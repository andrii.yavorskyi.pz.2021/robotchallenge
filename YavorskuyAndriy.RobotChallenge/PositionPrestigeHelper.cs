﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YavorskuyAndriy.RobotChallenge
{
    public class PositionPrestigeHelper
    {
        public static bool IsChecked { get; set; } = false;
        public static List<PositionPrestige> Evaluate(Map map)
        {
            List<PositionPrestige> positions = null;
            if (!IsChecked)
            {
                positions = RatePositions(map);
                IsChecked = true;
            }
            return positions;
        }
        private static List<PositionPrestige> RatePositions(Map map)
        {
            List<PositionPrestige> positions = new List<PositionPrestige>();
            for (int i = 2; i < 98; i++)
            {
                for (int j = 2; j < 98; j++)
                {
                    var position = new Position(i, j);
                    int potentialEnergy = map.GetNearbyResources(position, 2).Select(s => s.RecoveryRate).Sum();
                    positions.Add(new PositionPrestige(position, potentialEnergy));
                }
            }

            positions = positions.Where(x => x.EnergyProduce > 0).ToList();
            positions.Sort(new PositionComparer());
            positions.Reverse();
            return positions;
        }
    }
}
