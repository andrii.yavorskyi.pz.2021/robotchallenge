﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace YavorskuyAndriy.RobotChallenge.Test
{
    [TestClass]
    public class TestHelper
    {
        [TestMethod]
        public void TestMethodCollision()
        {
            Assert.IsTrue(PositionHelper.IsCollision(new Position(2, 2), new Position(3, 3)));
            Assert.IsFalse(PositionHelper.IsCollision(new Position(0, 0), new Position(1, 3)));
        }

        [TestMethod]
        public void TestRatePositions()
        {
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60}
                }
            };
            PositionHelper.EvaluateMap(map);
            Assert.AreEqual(PositionHelper.PositionPrestigeList.First().EnergyProduce, PositionHelper.PositionPrestigeList.Max(x => x.EnergyProduce));
            Assert.IsNull(PositionHelper.PositionPrestigeList.FirstOrDefault(x => x.EnergyProduce == 0));
        }

        [TestMethod]
        public void TestCalculateLoss()
        {
            Assert.AreEqual(8, PositionHelper.CalculateLoss(new Position(1, 2), new Position(3, 4)));
        }

        [TestMethod]
        public void TestFindBestPosition()
        {
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(5,3), RecoveryRate = 55},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60},
                    new EnergyStation(){Energy = 100, Position = new Position(2,7), RecoveryRate = 65},
                    new EnergyStation(){Energy = 100, Position = new Position(6,11), RecoveryRate = 80}
                }
            };

            PositionHelper.EvaluateMap(map);

            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() { Energy = 100, OwnerName = "test", Position = new Position(0, 0) },
                new Robot.Common.Robot() { Energy = 100, OwnerName = "test", Position = new Position(1, 1) },
                new Robot.Common.Robot() { Energy = 100, OwnerName = "Yavorskuy Andriy", Position = new Position(6, 10) }
            };

            var myRobot = robots.Where(r => r.OwnerName == "Yavorskuy Andriy").FirstOrDefault();
            var result = PositionHelper.FindBestPosition(myRobot, robots);
            Assert.IsTrue(PositionHelper.PositionPrestigeList.Exists(p => p.Position == result));
        }

        [TestMethod]
        public void TestEvaluateMap()
        {
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
        {
            new EnergyStation() { Energy = 100, Position = new Robot.Common.Position(0, 0), RecoveryRate = 50 },
            new EnergyStation() { Energy = 100, Position = new Robot.Common.Position(1, 1), RecoveryRate = 60 }
        }
            };

            PositionHelper.PositionPrestigeList.Clear();
            PositionHelper.EvaluateMap(map);
            Assert.AreEqual(96 * 96, PositionHelper.PositionPrestigeList.Count);
        }

    }
}
