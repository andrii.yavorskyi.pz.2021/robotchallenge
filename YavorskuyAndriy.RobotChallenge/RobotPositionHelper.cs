﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace YavorskuyAndriy.RobotChallenge
{
    public static class PositionHelper
    {
        private static Map _map;
        public static List<PositionPrestige> PositionPrestigeList { get; set; } = new List<PositionPrestige>();

        public static Position FindBestPosition(Robot.Common.Robot myRobot, IList<Robot.Common.Robot> robots)
        {
            Random r = new Random();

            List<PositionPrestige> nearestPositions = FindClosestPosition(myRobot, robots);

            int potentialEnergySum = _map.GetNearbyResources(myRobot.Position, 2).Select(s => s.RecoveryRate).Sum();
            nearestPositions.Add(new PositionPrestige(myRobot.Position, potentialEnergySum));
            if (nearestPositions.Count == 2)
            {
                if (potentialEnergySum >= nearestPositions.First().EnergyProduce)
                    return myRobot.Position;
                return nearestPositions.First().Position;
            }
            if (nearestPositions.Count == 1 && potentialEnergySum == 0)
            {
                List<PositionPrestige> bestPositions = PositionPrestigeList.Where(p => p.EnergyProduce > PositionPrestigeList.First().EnergyProduce * 0.85).ToList();
                bestPositions.Sort((p1, p2) => CalculateLoss(myRobot.Position, p1.Position).CompareTo(CalculateLoss(myRobot.Position, p2.Position)));

                List<Position> possibleX = GetPositionsByXCoordinate(myRobot.Position, bestPositions.Take(15).ElementAt(r.Next(Math.Min(15, bestPositions.Count))).Position);
                List<Position> bestX = possibleX.Where(p => CalculateLoss(myRobot.Position, p) <= myRobot.Energy / 2).ToList();
                return bestX.Last();
            }
            else
            {
                 var bestPostions = nearestPositions
                    .Where(p => p.EnergyProduce > nearestPositions.Max(np => np.EnergyProduce) * 0.85 ||
                                p.EnergyProduce >= 250).ToList();

                bestPostions.Sort((p1, p2) => CalculateLoss(myRobot.Position, p1.Position)
                    .CompareTo(CalculateLoss(myRobot.Position, p2.Position)));
                if (bestPostions.Any(p => p.Position == myRobot.Position))
                    return myRobot.Position;
                return bestPostions.Last().Position;
            }
        }

        public static void EvaluateMap(Map map)
        {
            if (PositionPrestigeList.Count == 0)
            {
                _map = map;
                PositionPrestigeList = PositionPrestigeHelper.Evaluate(map);
            }
        }

        private static List<PositionPrestige> FindClosestPosition(Robot.Common.Robot myRobot,
            IList<Robot.Common.Robot> robots)
        {
            List<PositionPrestige> result;
            double energyToMove = 0.5;

            do
            {
                result = FindClosestPositionByEnergy(myRobot, robots, energyToMove);
                energyToMove += 0.1;
                if (energyToMove > myRobot.Energy - 30)
                    break;
            } while (result.Count == 0);

            return result;
        }

        private static List<PositionPrestige> FindClosestPositionByEnergy(Robot.Common.Robot myRobot,
            IList<Robot.Common.Robot> robots, double energyToMove)
        {
            return PositionPrestigeList.Where(p =>
                CalculateLoss(myRobot.Position, p.Position) < (myRobot.Energy * energyToMove) && !YavorskuyAndriyAlgorithm.MyRobots
                    .Any(x => IsCollision(robots[x].Position, p.Position))).ToList();
        }

        public static bool IsCollision(Position p1, Position p2)
        {
            int x = Math.Abs(p1.X - p2.X);
            int y = Math.Abs(p1.Y - p2.Y);
            return (x < 3) && (y < 3);
        }

        private static List<Position> GetPositionsByXCoordinate(Position p1, Position p2)
        {
            List<Position> result = new List<Position>();
            int min = Math.Min(p1.X, p2.X);
            int max = Math.Max(p1.X, p2.X);

            for (int i = min + 1; i < max; i++)
            {
                int y = (i - p1.X) / (p2.X - p1.X) * (p2.Y - p1.Y) + p1.Y;
                result.Add(new Position(i, y));
            }

            return result;
        }
        private static int Min2D(int x1, int x2)
        {
            int[] nums =
            {
                (int) Math.Pow(x1 - x2, 2),
                (int) Math.Pow(x1 - x2 + 100, 2),
                (int) Math.Pow(x1 - x2 - 100, 2)
            };
            return nums.Min();
        }
        public static int CalculateLoss(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }
    }
    public class PositionPrestige
    {
        public PositionPrestige(Position position, int energyProduce)
        {
            Position = position;
            EnergyProduce = energyProduce;
        }
        public Position Position { get; private set; }
        public int EnergyProduce { get; private set; }
    }
}
