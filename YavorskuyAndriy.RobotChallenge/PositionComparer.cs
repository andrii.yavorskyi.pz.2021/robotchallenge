﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace YavorskuyAndriy.RobotChallenge
{
    public class PositionComparer : IComparer<PositionPrestige>
    {
        public int Compare(PositionPrestige x, PositionPrestige y)
        {
            return x.EnergyProduce.CompareTo(y.EnergyProduce);
        }
    }
}
