﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace YavorskuyAndriy.RobotChallenge.Test
{
    [TestClass]
    public class AlgorithmTest
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            YavorskuyAndriyAlgorithm algorithm = new YavorskuyAndriyAlgorithm();
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60}
                }
            };
            PositionHelper.EvaluateMap(map);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(1, 1)},
                new Robot.Common.Robot() {Energy = 100, OwnerName = "Yavorskuy Andriy", Position = new Position(8, 10)}
            };
            var command = algorithm.DoStep(robots, 2, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(new Position(2, 7), (command as MoveCommand).NewPosition);
        }

        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            YavorskuyAndriyAlgorithm algorithm = new YavorskuyAndriyAlgorithm();
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60}
                }
            };
            PositionHelper.EvaluateMap(map);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(1, 1)},
                new Robot.Common.Robot() {Energy = 1000, OwnerName = "Yavorskuy Andriy", Position = new Position(8, 10)}
            };
            int before = robots.Count;
            var command = algorithm.DoStep(robots, 2, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestCollectEnergyCommand()
        {
            YavorskuyAndriyAlgorithm algorithm = new YavorskuyAndriyAlgorithm();
            Map map = new Map()
            {
                Stations = new List<EnergyStation>()
                {
                    new EnergyStation(){Energy = 100, Position = new Position(0,0), RecoveryRate = 50},
                    new EnergyStation(){Energy = 100, Position = new Position(1,6), RecoveryRate = 60}
                }
            };
            PositionHelper.EvaluateMap(map);
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 100, OwnerName = "test", Position = new Position(1, 1)},
                new Robot.Common.Robot() {Energy = 100, OwnerName = "Yavorskuy Andriy", Position = new Position(1,5)}
            };
            var command = algorithm.DoStep(robots, 2, map);
            Assert.IsInstanceOfType(command, typeof(CollectEnergyCommand));
        }
    }
}
