﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YavorskuyAndriy.RobotChallenge
{
    public class YavorskuyAndriyAlgorithm : IRobotAlgorithm
    {
        private static int currentRound = 0;
        private static int myRobotCount = 10;
        public static SortedSet<int> MyRobots { get; } = new SortedSet<int>();

        public string Author
        {
            get { return "Yavorskuy Andriy"; }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            MyRobots.Add(robotToMoveIndex);
            Robot.Common.Robot myRobot = robots[robotToMoveIndex];

            if (myRobot.Energy > 200 && myRobotCount < 100 && MyRobots.Where(x =>
                x != robotToMoveIndex && PositionHelper.IsCollision(robots[x].Position, myRobot.Position)).Count() < 5)
            {
                myRobotCount++;
                return new CreateNewRobotCommand();
            }

            PositionHelper.EvaluateMap(map);
            Position possiblePosition = PositionHelper.FindBestPosition(myRobot, robots);
            int possibleEnergyCollected = map.GetNearbyResources(myRobot.Position, 2).Select(s => s.Energy).Sum();
            if (possiblePosition != myRobot.Position && possibleEnergyCollected < 100 && currentRound < 50)
            {
                return new MoveCommand()
                { NewPosition = possiblePosition };
            }

            return new CollectEnergyCommand();
        }
    }
}
