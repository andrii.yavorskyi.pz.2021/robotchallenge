﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace YavorskuyAndriy.RobotChallenge.Test
{
    [TestClass]
    public class TestPositionComparer
    {
        [TestMethod]
        public void TestCompare()
        {
            List<PositionPrestige> positions = new List<PositionPrestige>()
            {
                new PositionPrestige(new Robot.Common.Position(1, 2), 30),
                new PositionPrestige(new Robot.Common.Position(2, 4), 20),
                new PositionPrestige(new Robot.Common.Position(5, 1), 50)
            };

            List<PositionPrestige> result = new List<PositionPrestige>(){
                new PositionPrestige(new Robot.Common.Position(1, 2), 30),
                new PositionPrestige(new Robot.Common.Position(2, 4), 20),
                new PositionPrestige(new Robot.Common.Position(5, 1), 50)
            };
            result.Sort(new PositionComparer());
            Assert.AreEqual(positions[1].Position, result[0].Position);
            Assert.AreEqual(positions[0].Position, result[1].Position);
            Assert.AreEqual(positions[2].Position, result[2].Position);
        }
    }
}
